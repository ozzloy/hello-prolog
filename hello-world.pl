% team(school, lead, langauge, color, sponsor, place).

school(_School, [sjsu, de-anza, foothill, berkeley, ucsc]).

%% school(sjsu).
%% school(de-anza).
%% school(foothill).
%% school(uc-berkeley).
%% school(ucsc).

lead(alex).
lead(justin).
lead(nicole).
lead(tammy).
lead(steven).

language(haskell).
language(java).
language(javascript).
language(python).
language(cpp).

color(blue).
color(red).
color(green).
color(orange).
color(purple).

sponsor(apple).
sponsor(facebook).
sponsor(google).
sponsor(ibm).
sponsor(amazon).

% 1
ahead(blue, purple).
% 2
ahead(orange, haskell).
% 3
% team sponsored by ibm came in third
% how to more formally say 'third'?
third(_X) :- sponsored_by(ibm).
% 4
ahead(python, blue).
% 5
% team wearing red did not place first.
% \+ first(X) :- wearing(X, red).
%  how to say "not"?
ahead(google, blue).
ahead(haskell, javascript).
last(cpp).

goes_to(tammy, sjsu).

program_in(alex, java).

sponsored_by(justin, amazon).
sponsored_by(sjsu, facebook).
sponsored_by(foothill, ibm).
sponsored_by(nicole, google).

program_in(foothill, javascript).

wearing(berkeley, orange).

wearing(_Team, red) :- sponsored_by(_Team, apple).


parent(alice, bob).
parent(bob, carl).

%?- member(X, [a,b,c]).
%@ X = a ;
%@ X = b ;
%@ X = c.

%?- ['hello-world'].
%@ Warning: /home/ozzloy/src/prolog-stuff/hello-world/hello-world.pl:42:
%@ Warning:    Clauses of ahead/2 are not together in the source-file
%@ Warning:    Earlier definition at /home/ozzloy/src/prolog-stuff/hello-world/hello-world.pl:34
%@ Warning:    Current predicate: third/1
%@ Warning:    Use :- discontiguous ahead/2. to suppress this message
%@ Warning: /home/ozzloy/src/prolog-stuff/hello-world/hello-world.pl:60:
%@ Warning:    Clauses of program_in/2 are not together in the source-file
%@ Warning:    Earlier definition at /home/ozzloy/src/prolog-stuff/hello-world/hello-world.pl:53
%@ Warning:    Current predicate: sponsored_by/2
%@ Warning:    Use :- discontiguous program_in/2. to suppress this message
%@ Warning: /home/ozzloy/src/prolog-stuff/hello-world/hello-world.pl:64:
%@ Warning:    Singleton-marked variable appears more than once: _Team
%@ true.
%?- parent(alice, X), parent(X, Y).
%@ X = bob,
%@ Y = carl.

%?- parent(_,_).
%@ true ;
%@ true.
