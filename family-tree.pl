
ancestor(X, Y) :- parent(X, Z), ancestor(Z, Y).
ancestor(X, X).

parent(alex, zoe).

%?- ancestor(WHO, zoe).
%@ WHO = alex ;
%@ WHO = zoe.
%@ ERROR: Unknown procedure: parent/2
%@ ERROR: In:
%@ ERROR:   [11] parent(_14470,_14472)
%@ ERROR:   [10] ancestor(_14496,zoe) at /tmp/ediprologpnxgtv:2
%@ ERROR:    [9] <user>
%@    Exception: (11) parent(_7522, _14714) ? creep
%@    Exception: (10) ancestor(_7522, zoe) ? creep
