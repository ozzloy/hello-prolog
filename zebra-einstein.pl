/* GNU AGPLv3 (or later at your option)
 see bottom for more license info.
 please keep this notice. */

/*

problem statement from here:
https://web.stanford.edu/~laurik/fsmbook/examples/Einstein%27sPuzzle.html

Let us assume that there are five houses of different colors next to
each other on the same road. In each house lives a man of a different
nationality. Every man has his favorite drink, his favorite brand of
cigarettes, and keeps pets of a particular kind.

    1. The Englishman lives in the red house.

    2. The Swede keeps dogs.

    3. The Dane drinks tea.

    4. The green house is just to the left of the white one.

    5. The owner of the green house drinks coffee.

    6. The Pall Mall smoker keeps birds.

    7. The owner of the yellow house smokes Dunhills.

    8. The man in the center house drinks milk.

    9. The Norwegian lives in the first house.

    10. The Blend smoker has a neighbor who keeps cats.

    11. The man who smokes Blue Masters drinks bier.

    12. The man who keeps horses lives next to the Dunhill smoker.

    13. The German smokes Prince.

    14. The Norwegian lives next to the blue house.

    15. The Blend smoker has a neighbor who drinks water.


The question to be answered is: Who keeps fish?  */

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% 4. The green house is immediately to the left of the white one.
left(X, Y, [X, Y | _]).
left(X, Y, [_| Z]) :- left(X, Y, Z).

%?- left(x, y, []).
%@ false.

%?- left(x, y, [x]).
%@ false.

%?- left(x, y, [x, y]).
%@ true ;
%@ false.

%?- left(x, y, [z, x, y]).
%@ true ;
%@ false.

%?- left(x, y, [x, y, z]).
%@ true ;
%@ false.

%?- left(x, y, [y, x]).
%@ false.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% 8. The man in the center house drinks milk.
center(X, [_, _, X, _, _]).
%?- center(x, [_, _, x, _, _]).
%@ true.

%?- center(x, [_, _, y, _, _]).
%@ false.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% 9. The Norwegian lives in the first house.
first(X, [X, _, _, _, _]).
%?- first(x, [x, _, _, _, _]).
%@ true.

%?- first(x, [y, _, _, _, _]).
%@ false.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% 10. The Blend smoker has a neighbor who keeps cats.
neighbor(X, Y, Z) :- left(X, Y, Z).
neighbor(X, Y, Z) :- left(Y, X, Z).
neighbor(X, Y, [_ | Z]) :- neighbor(X, Y, Z).
%?- neighbor(x, y, [x, y, w]).
%@ true ;
%@ false.

%?- neighbor(x, y, [y, x, w]).
%@ true ;
%@ false.

%?- neighbor(x, y, [x, w, y]).
%@ false.

%?- neighbor(x, y, [y, w, x]).
%@ false.

%?- neighbor(x, y, [w, x, y]).
%@ true.

%?- neighbor(x, y, [w, y, x]).
%@ true.

%?- neighbor(x, y, [x, y, w]).
%@ true.

fiveHouses([_, _, _, _, _]).

% house(color, nationality, drink, smokes, pet)
% colors: red, green, white, yellow, blue
% nationality: english, swede, dane, norwegian, german
% drink: tea, coffee, milk, bier, water
% smokes: pall-mall, dunhill, blend, blue-masters, prince
% pet: dog, fish, bird, cat, horse

whoKeepsFish(X) :-
    fiveHouses(Houses)
    ,

    member(house(_, X, _, _, fish), Houses)
    ,

    % 1. The Englishman lives in the red house.
    member(house(red, english, _, _, _), Houses)
    ,

    % 2. The Swede keeps dogs.
    member(house(_, swede, _, _, dog), Houses)
    ,

    % 3. The Dane drinks tea.
    member(house(_, dane, tea, _, _), Houses)
    ,

    % 4. The green house is just to the left of the white one.
    left(house(green, _, _, _, _),
	 house(white, _, _, _, _),
	 Houses)
    ,

    % 5. The owner of the green house drinks coffee.
    member(house(green, _, coffee, _, _), Houses)
    ,

    % 6. The Pall Mall smoker keeps birds.
    member(house(_, _, _, pall-mall, bird), Houses)
    ,

    % 7. The owner of the yellow house smokes Dunhills.
    member(house(yellow, _, _, dunhills, _), Houses)
    , 

    % 8. The man in the center house drinks milk.
    center(house(_, _, milk, _, _), Houses)
    ,

    % 9. The Norwegian lives in the first house.
    first(house(_, norwegian, _, _, _), Houses)
    ,

    % 10. The Blend smoker has a neighbor who keeps cats.
    neighbor(house(_, _, _, blend,   _),
	     house(_, _, _,     _, cat),
	     Houses)
    ,

    % 11. The man who smokes Blue Masters drinks bier.
    member(house(_, _, bier, blue-masters, _), Houses)
    ,

    % 12. The man who keeps horses lives next to the Dunhill smoker.
    neighbor(house(_, _, _,       _, horse),
	     house(_, _, _, dunhill,     _),
	     Houses)
    ,

    % 13. The German smokes Prince.
    member(house(_, german, _, prince, _), Houses)
    ,

    % 14. The Norwegian lives next to the blue house.
    neighbor(house(   _, norwegian, _, _, _),
	     house(blue,         _, _, _, _),
	     Houses)
    ,

    % 15. The Blend smoker has a neighbor who drinks water.
    neighbor(house(_, _,     _, blend, _),
	     house(_, _, water,     _, _),
	     Houses)
.

%?- whoKeepsFish(X).
%@ false.

/*
 This file is part of prolog-hello-world.

 prolog-hello-world is free software: you can redistribute it and/or
 modify it under the terms of the GNU Affero General Public License as
 published by othe Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 prolog-hello-world is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public
 License along with prolog-hello-world.  If not, see
 <http://www.gnu.org/licenses/>.
*/
