parent(albert, bob).
parent(albert, betsy).
parent(albert, bill).

parent(alice, bob).
parent(alice, betsy).
parent(alice, bill).

parent(bob, carl).
parent(bob, charlie).

related(X, Y) :-
    parent(X, Y).
related(X, Y) :-
    parent(X, Z),
    related(Z, Y).
%?- related(albert, carl).
%@ true .


ancestor(X, Y) :- parent(X, Z), ancestor(Z, Y).
ancestor(X, X).

parent(alex, zoe).
%?- ancestor(WHO, zoe).
%@ WHO = alex ;
%@ WHO = zoe.
%?- parent(WHO, Z), ancestor(Z, zoe).
%@ WHO = alex,
%@ Z = zoe.
%?- ancestor(zoe, zoe).
%@ true.
%?- parent(zoe, Z), ancestor(Z, zoe).
%@ false.
