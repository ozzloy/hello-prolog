/* GNU AGPLv3 (or later at your option)
 see bottom for more license info.
 please keep this notice. */

/*

purpose of this file is to test out different constraints on
simplified zebra/einstein puzzles where constraints are added in.

these consist of an ordered set of things (like people), where each
thing has a set of properties (like what position or color the
person's house is).

constraints seen in other statements of this type:

neighbor, left, right, middle, first, not_first, last,

*/

/*
first, 2 people with 2 attributes.

with just two, the problem could make use of
  - left
  - right
  - first
  - not_first
  - last
*/

/*
bob and doug, the first two crew to the ISS on a falcon 9.

bob and doug have names, and positions on the crew dragon.

let's say bob sat to the left of doug when viewed from the front with
them facing the camera.  in other words, bob's left side and doug's
right side were next to each other.

1. bob sat to the left of doug
*/

twoCrew([_, _]).

left2(X, Y, [X, Y]).
%?- left2(a, b, C).
%@ C = [a, b].

%?- left2(x, y, [x, y]).
%@ true.

%?- left2(x, y, [y, x]).
%@ false.

%?- left2(x, y, [a, b]).
%@ false.

%?- left2(x, y, [x, a]).
%@ false.

%?- left2(x, y, [a, y]).
%@ false.

solve_seatingLeft2(Crew) :-
    twoCrew(Crew),

    % 1. bob sat to the left of doug
    left2(person(bob), person(doug), Crew)
.

%?- solve_seatingLeft2(Crew).
%@ Crew = [person(bob), person(doug)].


right2(X, Y, [Y, X]).
/*
1. doug sat to the right of bob
*/
solve_seatingRight2(Crew) :-
    twoCrew(Crew),

    % 1. doug sat to the right of bob
    right2(person(doug), person(bob), Crew),
    !
.

%?- solve_seatingRight2(Crew).
%@ Crew = [person(bob), person(doug)].

first2(X, [X | _]).

/*
1. bob sat first out of the two (where ordering is first left-to-right).
*/
solve_seatingFirst2(X, Crew) :-
    twoCrew(Crew),
    member(person(bob), Crew),
    member(person(doug), Crew),
    % 1. bob sat first out of the two
    %  (where ordering is first left-to-right).
    first2(person(bob), Crew),
    first2(X, Crew),
    !
.

%?- solve_seatingFirst2(X, Crew).
%@ X = person(bob),
%@ Crew = [person(bob), person(doug)].

/*
let's move on to a set of 3 people.

huey dewey louie
*/

threeThings([_, _, _]).

% X is somewhere to the left of Y
% X is not necessarily next to Y
left(X, Y, [X | Z]) :- member(Y, Z).
left(X, Y, [_ | Z]) :- left(X, Y, Z).
ahead(X, Y, [X | Tail]) :- member(Y, Tail).
ahead(X, Y, [_ | Tail]) :- ahead(X, Y, Tail).

%?- ahead(a, b, [c, b, a]).
%?- left(x, y, [x, y, z]).
%@ true.

%?- left(x, y, [x, z, y]).
%@ true.

%?- left(x, y, [x, y]).
%@ true.

%?- left(x, y, [y, x]).
%@ false.

%?- left(x, y, [z, x, y]).
%@ true.
/*
1. huey sat to the left of dewey
2. dewey sat to the left of louie
*/
solve_seatingLeft3(Ducks) :-
    threeThings(Ducks),

    % 1. huey sat to the left of dewey
    left(duck(huey), duck(dewey), Ducks),

    % 2. dewey sat to the left of louie
    left(duck(dewey), duck(louie), Ducks)
.

%?- solve_seatingLeft3(Ducks).
%@ Ducks = [duck(huey), duck(dewey), duck(louie)].

right(X, Y, Z) :- left(Y, X, Z).
%?- right(x, y, [x, y, z]).
%@ false.

%?- right(x, y, [y, x, z]).
%@ true.

/*
1. huey sat to the left of dewey
2. louie sat to the right of dewey
*/
seatingRight3(Ducks) :-
    threeThings(Ducks),

    % 1. huey sat to the left of dewey
    left(duck(huey), duck(dewey), Ducks),

    % 2. louie sat to the right of dewey
    right(duck(louie), duck(dewey), Ducks)
.
%?- solve_seatingRight3(Ducks).
%@ Ducks = [duck(huey), duck(dewey), duck(louie)].

middle3(X, [_, X, _]).
%?- middle3(x, [a, x, b]).
%@ true.

%?- middle3(x, [x, a, b]).
%@ false.

%?- middle3(x, [_, a, _]).
%@ false.

%?- middle3(x, [_, x, _]).
%@ true.

first(X, [X | _]).

/*
1. huey sat first (left-to-right order)
2. dewey sat in the middle
3. louie exists
*/
solve_seatingMiddle3(Ducks) :-
    threeThings(Ducks),

    % 1. huey sat first (left-to-right order)
    first(duck(huey), Ducks),

    % 2. dewey sat in the middle
    middle3(duck(dewey), Ducks),

    % 3. louie exists
    member(duck(louie), Ducks)
.
%?- solve_seatingMiddle3(Ducks).
%@ Ducks = [duck(huey), duck(dewey), duck(louie)].

/*
let's bring in another attribute, color!

according to
https://en.wikipedia.org/wiki/Huey,_Dewey,_and_Louie
"Huey is dressed in red, Dewey in blue, and Louie in green"

1. huey is first
2. the duck who wears blue is in the middle
3. the duck who wears blue is left of the duck who wears green
4. the duck who wears green is last
5. huey wears red.
6. louie is to the right of dewey

this rule is used for debugging
99. the ducks are seated as follows:
    (huey, red), (dewey, blue), (louie, green)
*/

% 4. the duck who wears green is last
%% built in last/2 has parameters in this order: last(List, Element)
%% last(X, [X]) :- !.
%% last(X, [_|Y]) :- last(X, Y).
%?- last(x, [x]).
%@ true.

%?- last(x, []).
%@ false.

%?- last(x, [x, y]).
%@ false.

%?- last(x, [y, x]).
%@ true.

%?- last(x, [y, z]).
%@ false.

completeSeating3([X, Y, Z], [X, Y, Z]).

solve_seatingWithColors3(Ducks) :-
    threeThings(Ducks),
    % 1. huey is first
    first(duck(huey, _), Ducks),

    % 2. the duck who wears blue is in the middle
    middle3(duck(_, blue), Ducks),

    % 3. the duck who wears blue is left of the duck who wears green
    left(duck(_, blue),
	 duck(_, green),
	 Ducks),

    % 4. the duck who wears green is last
    last(Ducks, duck(_, green)),

    % 5. huey wears red.
    member(duck(huey, red), Ducks),

    % 6. louie is to the right of dewey
    right(duck(louie, _),
	  duck(dewey, _),
	  Ducks)
/*
    ,

    % 99. the ducks are seated as follows:
    %    (huey, red), (dewey, blue), (louie, green)
    % this is used for troubleshooting
    completeSeating3([duck(huey, red),
                      duck(dewey, blue),
                      duck(louie, green)],
                     Ducks)
*/
.
%?- solve_seatingWithColors3(Ducks).
%@ Ducks = [duck(huey, red), duck(dewey, blue), duck(louie, green)] ;
%@ false.



/*
 This file is part of prolog-hello-world.

 prolog-hello-world is free software: you can redistribute it and/or
 modify it under the terms of the GNU Affero General Public License as
 published by othe Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 prolog-hello-world is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public
 License along with prolog-hello-world.  If not, see
 <http://www.gnu.org/licenses/>.
*/
