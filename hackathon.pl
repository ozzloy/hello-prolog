/* GNU AGPLv3 (or later at your option)
 see bottom for more license info.
 please keep this notice. */

/*

your task is to write a prolog knowledge base to solve the following
logic puzzle.

Five teams from 5 California colleges

(SJSU, De Anza, Foothill, UC Berkeley and UC Santa Cruz)

are participating in a hackathon.

Each team has a team lead:

Alex, Justin, Nicole, Tammy and Steven.

Each team is using a different programming language:

Haskell, Java, JavaScript, Python and C++.

Each team has chosen a different color for the T-shirts they are
wearing during the hackathon:

blue, red, green, orange and purple.

Five companies are sponsoring these teams:

Apple, Facebook, Google, IBM and Amazon.

The results are not out yet but the following information has been
leaked:

a1. The team wearing blue placed ahead of the team wearing purple.

a2. The team wearing orange placed ahead of the team programming in
Haskell.

a3. The team sponsored by IBM came in third.

a4. The team programming in Python placed ahead of the team wearing
blue.

a5. The team wearing red did not place first.

a6. The team sponsored by Google placed ahead of the team wearing blue.

a7. The team programming in Haskell placed ahead of the team
programming in JavaScript.

a8. The last team used C++.

We also know that:

b1. Tammy goes to SJSU

b2. Alex will only program in Java.

b3. Justin's team is sponsored by Amazon.

b4. The SJSU team is sponsored by Facebook.

b5. The Foothill team is sponsored by IBM.

b6. Nicole's team is sponsored by Google.

b7. The Foothill team programs in JavaScript.

b8. The Berkeley team is wearing orange.

b9. The team sponsored by Apple is wearing red.

b10. The UC Santa Cruz team is wearing purple.

b11. The SJSU team is wearing blue.

Question 1:

Please write the Prolog predicate winners to answer the following
question:

Which colleges came in first and second place?

?- winners(First, Second).

Question 2:

Please write the Prolog predicate solution to answer the following
question:

What are the detailed results of the hackathon? The teams should be
listed in order with the following information: college, team lead,
programming language, sponsor and T-shirt color.

?- solution(Results).

*/

% 5 teams
% colleges: sjsu, de_anza, foothill, uc_berkeley, ucsc
% leads: alex, justin, nicole, tammy, steven
% languages: haskell, java, js, python, cpp
% colors: blue, red, green, orange, purple
% sponsors: apple, facebook, google, ibm, amazon

% team(college, lead, language, color, sponsor)
% possible: team(sjsu, justin, orange, amazon)

% use length(SomeList, SomeLength). instead
% fiveTeams([_, _, _, _, _]).

ahead(X, Y, [X|Z]) :- member(Y, Z).
ahead(X, Y, [_|W]) :- ahead(X, Y, W).
%?- ahead(a, b, [a, b]).
%@ true.
%?- ahead(b, a, [a, b]).
%@ false.
%?- ahead(a, c, [a, b, c]).
%@ true.
%?- ahead(a, b, [a]).
%@ false.

first(X, [X, _, _, _, _]).
%?- first(x, [x, a, b, c, d]).
%@ true.
%?- first(x, [a, x, b, c, d]).
%@ false.
second(X, [_, X, _, _, _]).
%?- second(x, [a, x, b, c, d]).
%@ true.
%?- second(x, [x, a, b, c, d]).
%@ false.
third( X, [_, _, X, _, _]).
%?- third(x, [a, b, x, c, d]).
%@ true.
%?- third(x, [x, a, b, c, d]).
%@ false.
fourth(X, [_, _, _, X, _]).
%?- fourth(x, [a, b, c, x, d]).
%@ true.
%?- fourth(x, [x, a, b, c, d]).
%@ false.
fifth( X, [_, _, _, _, X]).
%?- fifth(x, [a, b, c, d, x]).
%@ true.
%?- fifth(x, [x, a, b, c, d]).
%@ false.

not_first(X, [Y | _]) :- X \= Y.
%?- not_first(x, [x, a, b, c, d]).
%@ false.
%?- not_first(x, [a, x, b, c, d]).
%@ true.
%?- not_first(x, [a, b, x, c, d]).
%@ true.
%?- not_first(x, [a, b, c, x, d]).
%@ true.
%?- not_first(x, [a, b, c, d, x]).
%@ true.

/*
13:17 < ozzloy> k
13:20 < anniepoo> back
13:20 < anniepoo> 8cD
13:21 < anniepoo> ok, soooo
13:21 < anniepoo> yeah, I lived in a hippy commune in the 80s
13:22 < anniepoo> okey dokey - so, re avoiding uc_berkeley vs berkeley
13:22 < anniepoo> you could do something like
13:22 < anniepoo> maplist(valid_team,  Teams)
13:23 < anniepoo> valid_team(team(College, Lead, Language, Color, Sponsor)) :-  
13:24 < anniepoo>                        member(College, [uc_berkeley, de_anza, 
                  ...  ]),
13:24 < anniepoo>             member(Lead, [.... whatever the leads are....]),
13:24 < anniepoo> and so on
13:25 < anniepoo> if you had written this progra in the 8 lines actually 
                  needed, instead of all that comment noise, I bet it would 
                  have been obvious though
13:25 < anniepoo> 300 lines of prolog is a LOT of prolog
13:26 < anniepoo> you there?
*/

valid_team(team(College, Lead, Language, Color, Sponsor)) :-
    member(College, [sjsu, de_anza, foothill, uc_berkeley, ucsc]),
    member(Lead, [alex, justin, nicole, tammy, steven]),
    member(Language, [haskell, java, js, python, cpp]),
    member(Color, [blue, red, green, orange, purple]),
    member(Sponsor, [apple, facebook, google, ibm, amazon]).

winners(First, Second) :-

    % 5 teams
    length(Teams, 5),
    %maplist(valid_team, Teams)

    first(First, Teams),

    second(Second, Teams),

    % %----------------------------------------------------------------
    % % colleges: sjsu, de_anza, foothill, uc_berkeley, ucsc
    % %----------------------------------------------------------------
    % member(team(       sjsu, _, _, _, _), Teams),
    member(  team(    de_anza, _, _, _, _), Teams),
    % member(team(   foothill, _, _, _, _), Teams),
    % member(team(uc_berkeley, _, _, _, _), Teams),
    % member(team(       ucsc, _, _, _, _), Teams),

    % %----------------------------------------------------------------
    % % leads: alex, justin, nicole, tammy, steven
    % %----------------------------------------------------------------
    % member(team(_,   alex, _, _, _), Teams),
    % member(team(_, justin, _, _, _), Teams),
    % member(team(_, nicole, _, _, _), Teams),
    % member(team(_,  tammy, _, _, _), Teams),
    member(  team(_, steven, _, _, _), Teams),

    % %----------------------------------------------------------------
    % % languages: haskell, java, js, python, cpp
    % %----------------------------------------------------------------
    % member(team(_, _, haskell, _, _), Teams),
    % member(team(_, _,    java, _, _), Teams),
    % member(team(_, _,      js, _, _), Teams),
    % member(team(_, _,  python, _, _), Teams),
    % member(team(_, _,     cpp, _, _), Teams),

    % %----------------------------------------------------------------
    % % colors: blue, red, green, orange, purple
    % %----------------------------------------------------------------
    % member(team(_, _, _,   blue, _), Teams),
    % member(team(_, _, _,    red, _), Teams),
    member(  team(_, _, _,  green, _), Teams),
    % member(team(_, _, _, orange, _), Teams),
    % member(team(_, _, _, purple, _), Teams),

    % %----------------------------------------------------------------
    % % sponsors: apple, facebook, google, ibm, amazon
    % %----------------------------------------------------------------
    % member(team(_, _, _, _,    apple), Teams),
    % member(team(_, _, _, _, facebook), Teams),
    % member(team(_, _, _, _,   google), Teams),
    % member(team(_, _, _, _,      ibm), Teams),
    % member(team(_, _, _, _,   amazon), Teams),


    %----------------------------------------------------------------
    % generically: team(college,   lead, language,  color, sponsor)
    %  example:    team(   sjsu, justin,       js, orange,  amazon)
    %----------------------------------------------------------------

    %----------------------------------------------------------------
    % b1. tammy goes to sjsu
    member(team(sjsu, tammy, _, _, _), Teams),

    % b2. alex programs java
    member(team(_, alex, java, _, _), Teams),

    % b3. amazon sponsors justin
    member(team(_, justin, _, _, amazon), Teams),

    % b4. facebook sponsors sjsu
    member(team(sjsu, _, _, _, facebook), Teams),

    % b5. ibm sponsors foothill
    member(team(foothill, _, _, _, ibm), Teams),

    % b6. google sponsors nicole
    member(team(_, nicole, _, _, google), Teams),

    % b7. foothill uses js
    member(team(foothill, _, js, _, _), Teams),

    % b8. berkeley is orange
    member(team(uc_berkeley, _, _, orange, _), Teams),

    % b9. apple sponsors red
    member(team(_, _, _, red, apple), Teams),

    % b10. ucsc is purple
    member(team(ucsc, _, _, purple, _), Teams),

    % b11. sjsu is blue
    member(team(sjsu, _, _, blue, _), Teams),
    %----------------------------------------------------------------

    %----------------------------------------------------------------
    % a1. blue team places ahead of purple team
    ahead(team(_, _, _,   blue, _),
	  team(_, _, _, purple, _),
	  Teams),

    % a2. orange team placed ahead of haskell team
    ahead(team(_, _,       _, orange, _),
	  team(_, _, haskell,      _, _),
	  Teams),

    % a3. ibm team is third
    third(team(_, _, _, _, ibm), Teams),

    % a4. python team ahead of blue team
    ahead(team(_, _, python,    _, _),
	  team(_, _,      _, blue, _),
	  Teams),

    % a5. red team is not first
    not_first(team(_, _, _, red, _), Teams),

    % a6. google team ahead of blue team
    ahead(team(_, _, _,    _, google),
	  team(_, _, _, blue,      _),
	  Teams),

    % a7. haskell team ahead of js team (no surprise there)
    ahead(team(_, _, haskell, _, _),
	  team(_, _,      js, _, _),
	  Teams),

    % a8. last team used cpp (no surprise there either)
    fifth(team(_, _, cpp, _, _), Teams),

    maplist(valid_team, Teams)
.
%----------------------------------------------------------------


% ?- winners(First, Second).
%@ First = team(uc_berkeley, nicole, python, orange, google),
%@ Second = team(sjsu, tammy, haskell, blue, facebook) ;
%@ false.

/*
 This file is part of prolog-hello-world.

 prolog-hello-world is free software: you can redistribute it and/or
 modify it under the terms of the GNU Affero General Public License as
 published by othe Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 prolog-hello-world is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public
 License along with prolog-hello-world.  If not, see
 <http://www.gnu.org/licenses/>.
*/

my_sum([], 0).
my_sum([Head|Tail], Sum) :-
    my_sum(Tail, TailSum),
    Sum is TailSum + Head
.

% ?- my_sum([], X).
%@ X = 0.
