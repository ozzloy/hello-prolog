
ahead2(X, Y, [X, Y]).

twoTeams([_, _]).

solution2(TeamA, TeamB) :-
    twoTeams(AllTeams),
    ahead2(TeamA, TeamB, AllTeams),
    member(TeamA, AllTeams),
    member(TeamB, AllTeams),
    ahead2(team(a), team(b), AllTeams),
    member(team(a), AllTeams),
    member(team(b), AllTeams).

%?- solution2(TeamA, TeamB).
%@ Correct to: "solution2(TeamA,TeamB)"? yes
%@ TeamA = team(a),
%@ TeamB = team(b) ;
%@ false.



ahead3(X, Y, [X, Y, _]).
ahead3(X, Y, [X, _, Y]).
ahead3(X, Y, [_, X, Y]).

threeTeams([_, _, _]).

solution3(TeamA, TeamB, TeamC) :-
    threeTeams(AllTeams),
    ahead3(TeamA, TeamB, AllTeams),
    ahead3(TeamB, TeamC, AllTeams),
    member(TeamA, AllTeams),
    member(TeamB, AllTeams),
    member(TeamC, AllTeams),
    member(team(a), AllTeams),
    member(2, AllTeams),
    member(3, AllTeams),
    ahead3(2, 3, AllTeams),
    ahead3(team(a), 3, AllTeams)
.

%?- solution3(TeamA, TeamB, TeamC).
%@ Correct to: "solution3(TeamA,TeamB,TeamC)"? yes
%@ TeamA = team(a),
%@ TeamB = 2,
%@ TeamC = 3 ;
%@ TeamA = 2,
%@ TeamB = team(a),
%@ TeamC = 3 ;
%@ false.


ahead(X, Y, [X|Z]) :- member(Y, Z), !.
ahead(X, Y, [_|W]) :- ahead(X, Y, W).

%?- ahead(a, b, [a, b]).
%@ true.
%?- ahead(a, c, [a, b, c]).
%@ true.
%?- ahead(a, b, [c, a, b]).
%@ true.
