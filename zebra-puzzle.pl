/* GNU AGPLv3 (or later at your option)
 see bottom for more license info.
 please keep this notice. */

%--------------------------------------------------------------------
% 5 houses of different colors, next to each other on main street
% each house has a different person, different profession
%  professions:
%    - teacher
%    - programmer
%    - dentist
%    - carpenter
%    - engineer
% each person has a different pet:
%  pets:
%    - dog
%    - cat
%    - llama
%    - horse
%    - zebra
% each has a favorite drink:
%  drinks:
%    - water
%    - coffee
%    - tea
%    - orangejuice
%    - milk
% each has a car:
%  cars:
%    - buick
%    - ford
%    - honda
%    - toyota
%    - vw
% houses have color:
%  colors:
%    - red
%    - purple
%    - blue
%    - green
%    - yellow
%--------------------------------------------------------------------

%--------------------------------------------------------------------
% 1. teacher lives in red house
% 2. dentist owns dog
% 3. green house drinks coffee
% 4. programmer drinks tea
% 5. green to right of purple
% 6. honda driver has llama
% 7. ford driver is yellow house
% 8. carpenter lives first house on left [carpenter, _, _, _, _]
% 9. vw next to cat
% 10. toyota drinks orangejuice
% 11. [_, _, X, _, _] drinks milk
% 12. ford driver lives in house next to horse
%     [... ford, horse ...] or [... horse, ford ...]
% 13. engineer drives buick
% 14. [... carpenter, blue ...] or [... blue, carpenter ...]
% 15. [... vw, water ...] or [... water, vw ...]
%--------------------------------------------------------------------

%--------------------------------------------------------------------
% who owns zebra, who drinks water?
%--------------------------------------------------------------------

%--------------------------------------------------------------------
% house(profession, color, pet, drink, car)
% possible example:
%  house(teacher, red, zebra, water, honda)
%--------------------------------------------------------------------

%--------------------------------------------------------------------
% 5. green to right of purple
%--------------------------------------------------------------------
% true if Y follows X in the list L
% right_of(X, Y, L).
right(X, Y, [X, Y | _]).
right(X, Y, [_ | W]) :- right(X, Y, W).

%--------------------------------------------------------------------
% 11. [_, _, X, _, _] drinks milk
%--------------------------------------------------------------------
middle(X, [_, _, X, _, _]).

%--------------------------------------------------------------------
% 8. carpenter lives first house on left [carpenter, _, _, _, _]
%--------------------------------------------------------------------
first(X, [X | _]).

%--------------------------------------------------------------------
% 9. vw next to cat
%--------------------------------------------------------------------
next(X, Y, L) :- right(X, Y, L).
next(X, Y, L) :- right(Y, X, L).

%--------------------------------------------------------------------
% from main problem statement
%--------------------------------------------------------------------
fiveHouses([_, _, _, _, _]).

solution(ZebraOwner, WaterDrinker) :-
    fiveHouses(MainStreet)
    ,

    member(house(ZebraOwner, _, zebra, _, _), MainStreet)
    ,

    member(house(WaterDrinker, _, _, water, _), MainStreet)
    ,
    
    % 1. teacher lives in red house
    member(house(teacher, red, _, _, _), MainStreet)
    ,

    % 2. dentist owns dog
    member(house(dentist, _, dog, _, _), MainStreet)
    ,

    % 3. green house drinks coffee
    member(house(_, green, _, coffee, _), MainStreet)
    ,

    % 4. programmer drinks tea
    member(house(programmer, _, _, tea, _), MainStreet)
    ,

    % 5. green to right of purple
    right(house(_, purple, _, _, _),
	  house(_,  green, _, _, _),
	  MainStreet)
    ,

    % 6. honda driver has llama
    member(house(_, _, llama, _, honda), MainStreet)
    ,

    % 7. ford driver is yellow house
    member(house(_, yellow, _, _, ford), MainStreet)
    ,

    % 8. carpenter lives first house on left [carpenter, _, _, _, _]
    first(house(carpenter, _, _, _, _), MainStreet)
    ,

    % 9. vw next to cat
    next(house(_, _,   _, _, vw),
	 house(_, _, cat, _,  _),
	 MainStreet)
    ,

    % 10. toyota drinks orangejuice
    member(house(_, _, _, orange-juice, toyota), MainStreet)
    ,

    % 11. [_, _, X, _, _] drinks milk
    middle(house(_, _, _, milk, _), MainStreet)
    ,

    % 12. ford driver lives in house next to horse
    %     [... ford, horse ...] or [... horse, ford ...]
    next(house(_, _,     _, _, ford),
	 house(_, _, horse, _,    _),
	 MainStreet)
    ,

    % 13. engineer drives buick
    member(house(engineer, _, _, _, buick), MainStreet),

    % 14. [... carpenter, blue ...] or [... blue, carpenter ...]
    next(house(        _, blue, _, _, _),
	 house(carpenter,    _, _, _, _),
	 MainStreet)
    ,

    % 15. [... vw, water ...] or [... water, vw ...]
    next(house(_, _, _,     _, vw),
	 house(_, _, _, water,  _),
	 MainStreet)
.

%?- solution(ZebraOwner, WaterDrinker).
%@ ZebraOwner = engineer,
%@ WaterDrinker = carpenter ;
%@ false.

%% solveZO_WD(ZebraOwner, WaterDrinker) :-
%%     member(house(ZebraOwner, _, zebra, _, _), MainStreet)
%%     ,

%%     member(house(WaterDrinker, _, _, water, _), MainStreet)
%%     ,

%%     clues(MainStreet)
%% .

%?- solveZO_WD(ZebraOwner, WaterDrinker). 
solveAll(MainStreet) :-
    fiveHouses(MainStreet)
    ,

    member(house(_, _, zebra, _, _), MainStreet)
    ,
    
    % 1. teacher lives in red house
    member(house(teacher, red, _, _, _), MainStreet)
    ,

    % 2. dentist owns dog
    member(house(dentist, _, dog, _, _), MainStreet)
    ,

    % 3. green house drinks coffee
    member(house(_, green, _, coffee, _), MainStreet)
    ,

    % 4. programmer drinks tea
    member(house(programmer, _, _, tea, _), MainStreet)
    ,

    % 5. green to right of purple
    right(house(_, purple, _, _, _),
	  house(_,  green, _, _, _),
	  MainStreet)
    ,

    % 6. honda driver has llama
    member(house(_, _, llama, _, honda), MainStreet)
    ,

    % 7. ford driver is yellow house
    member(house(_, yellow, _, _, ford), MainStreet)
    ,

    % 8. carpenter lives first house on left [carpenter, _, _, _, _]
    first(house(carpenter, _, _, _, _), MainStreet)
    ,

    % 9. vw next to cat
    next(house(_, _,   _, _, vw),
	 house(_, _, cat, _,  _),
	 MainStreet)
    ,

    % 10. toyota drinks orangejuice
    member(house(_, _, _, orange-juice, toyota), MainStreet)
    ,

    % 11. [_, _, X, _, _] drinks milk
    middle(house(_, _, _, milk, _), MainStreet)
    ,

    % 12. ford driver lives in house next to horse
    %     [... ford, horse ...] or [... horse, ford ...]
    next(house(_, _,     _, _, ford),
	 house(_, _, horse, _,    _),
	 MainStreet)
    ,

    % 13. engineer drives buick
    member(house(engineer, _, _, _, buick), MainStreet),

    % 14. [... carpenter, blue ...] or [... blue, carpenter ...]
    next(house(        _, blue, _, _, _),
	 house(carpenter,    _, _, _, _),
	 MainStreet)
    ,

    % 15. [... vw, water ...] or [... water, vw ...]
    next(house(_, _, _,     _, vw),
	 house(_, _, _, water,  _),
	 MainStreet)
.

/*
%?- solveAll(MainStreet).
%@ MainStreet =
 [house( carpenter, yellow,   cat,       water,   ford),
  house(programmer,   blue, horse,         tea,     vw),
  house(   teacher,    red, llama,        milk,  honda),
  house(   dentist, purple,   dog, orangejuice, toyota),
  house(  engineer,  green, zebra,      coffee,  buick)] ;
%@ false.
*/

/*
 This file is part of prolog-hello-world.

 prolog-hello-world is free software: you can redistribute it and/or
 modify it under the terms of the GNU Affero General Public License as
 published by othe Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 prolog-hello-world is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public
 License along with prolog-hello-world.  If not, see
 <http://www.gnu.org/licenses/>.
*/
